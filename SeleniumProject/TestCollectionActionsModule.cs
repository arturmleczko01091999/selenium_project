﻿using System.Collections.ObjectModel;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumProject.BaseClass;
using SeleniumProject.Elements;
using SeleniumProject.Extensions;
using SeleniumProject.Functions;
using SeleniumProject.Helpers;
using SeleniumProject.Utility;

namespace SeleniumProject
{
    [TestFixture]
    public class TestCollectionActionsModule : BaseTest
    {
        [Test, Category("Remove collection")]
        [Description("Test of collection removal")]
        public void TestRemoveCollection()
        {
            CollectionActionsModuleElements elements = new CollectionActionsModuleElements(driver, 5000);

            string collectionName = RandomWordHelper.RandomWord(20);

            LoginFunction.Login(driver, LoginData.email, LoginData.password);
            CreateCollectionFunction.CreateCollection(driver, collectionName);

            GoToHelper.GoTo(driver, PageReference.Collections);

            elements.CollectionListElement.Click();
            elements.EditCollectionButtonOnEditColletcionPage.Click();
            elements.DeleteCollectionButtonOnEditModal.ClickWithWait();
            elements.ConfirmDeleteCollectionButton.ClickWithWait();

            bool deletedCollectionExists = IsElementPresentHelper.IsElementPresent(driver, By.XPath($".//a[normalize-space() = '{collectionName}']"));
            Assert.IsFalse(deletedCollectionExists);
        }

        [Test, Category("Duplicate collection")]
        [Description("Tests duplicate collections and validates them")]
        public void TestDuplicateCollection()
        {
            CollectionActionsModuleElements elements = new CollectionActionsModuleElements(driver, 5000);

            string collectionName = RandomWordHelper.RandomWord(20);

            LoginFunction.Login(driver, LoginData.email, LoginData.password);
            CreateCollectionFunction.CreateCollection(driver, collectionName);

            GoToHelper.GoTo(driver, PageReference.Collections);

            elements.CollectionActionsButton.Click();
            elements.DuplicateCollectionButton.Click();

            Thread.Sleep(1500);
            ReadOnlyCollection<IWebElement> collectionsOfTheSameName = driver.FindElements(By.XPath($".//h6[normalize-space() = '{collectionName}']"));

            Assert.AreEqual(2, collectionsOfTheSameName.Count);
        }
    }
}
