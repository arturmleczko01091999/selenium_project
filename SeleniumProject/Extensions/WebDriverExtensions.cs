﻿using System;
using System.Collections.ObjectModel;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace SeleniumProject.Extensions
{
    public static class WebDriverExtensions
    {
        public static IWebElement FindElement(this IWebDriver driver, By by, int timeoutInSeconds)
        {
            if (timeoutInSeconds > 0)
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));
                return wait.Until(driver => driver.FindElement(by));
            }
            return driver.FindElement(by);
        }

        public static ReadOnlyCollection<IWebElement> FindElements(this IWebDriver driver, By by, int timeoutInSeconds)
        {
            if (timeoutInSeconds > 0)
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));
                return wait.Until(driver => driver.FindElements(by));
            }
            return driver.FindElements(by);
        }

        public static void ClickWithWait(this IWebElement element)
        {
            int iterationCounter = 0;

            try
            {
                element.Click();
            }
            catch
            {
                iterationCounter++;
                do
                {
                    iterationCounter++;
                    Thread.Sleep(1000);
                } while (!element.Displayed && iterationCounter < 30);

                element.Click();
            }
        }

        public static void SendKeysWithWait(this IWebElement element, string text)
        {
            int iterationCounter = 0;

            try
            {
                element.SendKeys(text);
            }
            catch
            {
                iterationCounter++;
                do
                {
                    iterationCounter++;
                    Thread.Sleep(1000);
                } while (!element.Displayed && iterationCounter < 30);

                element.SendKeys(text);
            }
        }
    }
}
