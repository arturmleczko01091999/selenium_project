﻿using NUnit.Framework;
using SeleniumProject.BaseClass;
using SeleniumProject.Elements;
using SeleniumProject.Extensions;
using SeleniumProject.Functions;
using SeleniumProject.Helpers;
using SeleniumProject.Utility;

namespace SeleniumProject
{
    [TestFixture]
    public class TestCreateCollectionModule : BaseTest
    {
        [Test, Category("Create collection")]
        [Description("Test of adding a collection from the collection page")]
        public void CreateCollection()
        {
            string collectionName = RandomWordHelper.RandomWord(20);

            LoginFunction.Login(driver, LoginData.email, LoginData.password);
            CreateCollectionFunction.CreateCollection(driver, collectionName);

            CheckCollectionExistsFunction.CheckCollectionExists(driver, collectionName, 1500);
        }

        [Test, Category("Create collection")]
        [Description("Test of adding a collection from a modal")]
        public void CreateCollectionOnModal()
        {
            CreateCollectionModuleElements elements = new CreateCollectionModuleElements(driver, 5000);
            
            LoginFunction.Login(driver, LoginData.email, LoginData.password);
            GoToHelper.GoTo(driver, PageReference.ProductList);

            elements.CreateNewCollectionButtonOnProductListPage.ClickWithWait();
            elements.CreateNewCollectionButtonOnProductListPageModal.ClickWithWait();

            string collectionName = RandomWordHelper.RandomWord(8);
            elements.CollectionNameInputOnProductListPageModal.SendKeys(collectionName);
            elements.SaveCollectionButtonOnProductListPageModal.ClickWithWait();

            GoToHelper.GoTo(driver, PageReference.Collections, 1500);
            CheckCollectionExistsFunction.CheckCollectionExists(driver, collectionName, 1500);
        }
    }
}
