﻿using NUnit.Framework;
using SeleniumProject.BaseClass;
using SeleniumProject.Utility;
using SeleniumProject.Elements;
using SeleniumProject.Functions;

namespace SeleniumProject
{
    [TestFixture]
    public class TestLoginModule : BaseTest
    {
        [Test, Category("Login module")]
        [Description("Tests the correctness of the url after successful login")]
        public void TestSuccessfulLogin()
        {
            LoginFunction.Login(driver, LoginData.email, LoginData.password);

            string actualURL = driver.Url;
            string expectedURL = "https://januszpv.pl/";

            Assert.AreEqual(expectedURL, actualURL);
        }

        [Test, Category("Login module")]
        [Description("Test of correctness of url and toast message after invalid login")]
        public void TestInvalidLogin()
        {
            LoginModuleElements elements = new LoginModuleElements(driver);

            string invalidEmail = "invalid-email@invalid.com";
            string invalidPassword = "invalid-password@invalid.com";

            LoginFunction.Login(driver, invalidEmail, invalidPassword);

            string actualURL = driver.Url;
            string expectedURL = "https://januszpv.pl/login&msg=w_loginFailed";

            string actualToasMessage = elements.TestAlertMessage.Text;
            string expectedToastMessage = "Podano niepoprawny login lub hasło";

            Assert.AreEqual(expectedURL, actualURL);
            Assert.AreEqual(expectedToastMessage, actualToasMessage);
        }
    }
}
