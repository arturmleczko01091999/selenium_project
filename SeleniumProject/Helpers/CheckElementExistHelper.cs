﻿using System;
using System.Collections.ObjectModel;
using OpenQA.Selenium;

namespace SeleniumProject.Helpers
{
    public static class CheckElementExistHelper
    {
        public static void CheckElementExist(IWebElement element)
        {
            if (!element.Displayed)
            {
                throw new Exception("Element does not exist");
            }
        }

        public static void CheckElementsExist(ReadOnlyCollection<IWebElement> elements)
        {
            if (elements.Count == 0)
            {
                throw new Exception("No elements");
            }
        }
    }
}
