﻿using OpenQA.Selenium;

namespace SeleniumProject.Helpers
{
    public static class IsElementPresentHelper
    {
        public static bool IsElementPresent(IWebDriver driver, By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
    }
}
