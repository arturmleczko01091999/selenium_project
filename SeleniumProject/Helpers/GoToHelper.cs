﻿using System.Threading;
using OpenQA.Selenium;

namespace SeleniumProject.Helpers
{
    public enum PageReference
    {
        ProductList,
        UnshippedCarts,
        Collections
    }

    public static class GoToHelper
    {
        private static string GetURLFromPageReference(this PageReference pageReference)
        {
            switch (pageReference)
            {
                case PageReference.ProductList:
                    return "https://januszpv.pl/pl,categoryView,panele_monokrystaliczne,108,.html";
                case PageReference.UnshippedCarts:
                    return "https://januszpv.pl/pl,basket,,0,0.html";
                case PageReference.Collections:
                    return "https://januszpv.pl/pl,collectionView,,0,0.html";
                default:
                    return "no url";
            }
        }

        public static void GoTo(IWebDriver driver, PageReference pageReference, int waitTime = 0)
        {
            if (waitTime != 0)
            {
                Thread.Sleep(waitTime);
            }

            string pageURL = GetURLFromPageReference(pageReference);
            driver.Navigate().GoToUrl(pageURL);
        }
    }
}
