﻿using System;
using System.Linq;

namespace SeleniumProject.Helpers
{
    public class RandomWordHelper
    {
        private static Random random = new Random();

        public static string RandomWord(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
