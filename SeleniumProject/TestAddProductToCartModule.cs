﻿using NUnit.Framework;
using SeleniumProject.BaseClass;
using SeleniumProject.Helpers;
using SeleniumProject.Utility;
using SeleniumProject.Elements;
using SeleniumProject.Functions;

namespace SeleniumProject
{
    [TestFixture]
    public class TestAddProductToCartModule : BaseTest
    {
        [Test, Category("Add product to cart module")]
        [Description("Test adding a product to the cart from the product list page")]
        public void TestAddProductToCartFromProductListPage()
        {
            AddProductToCartModuleElements elements = new AddProductToCartModuleElements(driver);

            LoginFunction.Login(driver, LoginData.email, LoginData.password);
            GoToHelper.GoTo(driver, PageReference.ProductList);

            CheckElementExistHelper.CheckElementsExist(elements.AddToCartButtonsOnCategoryView);

            int productIndex = 0;
            string productName = elements.ProductNameElementOnCategoryPage.Text;

            elements.AddToCartButtonsOnCategoryView[productIndex].Click();

            AddProductToCartFunction.AddProductToCart(driver, productName);
        }

        [Test, Category("Add product to cart module")]
        [Description("Test of adding products from a product card")]
        public void TestAddProductToCartFromProductCard()
        {
            AddProductToCartModuleElements elements = new AddProductToCartModuleElements(driver);

            LoginFunction.Login(driver, LoginData.email, LoginData.password);
            GoToHelper.GoTo(driver, PageReference.ProductList);

            CheckElementExistHelper.CheckElementExist(elements.ProductListElement);

            elements.ProductListElement.Click();

            string productName = elements.ProductNameOnProductPage.Text;

            elements.AddToCartButtonOnProductPage.Click();

            AddProductToCartFunction.AddProductToCart(driver, productName);
        }
    }
}