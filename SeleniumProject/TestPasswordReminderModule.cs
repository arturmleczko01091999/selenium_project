﻿using NUnit.Framework;
using SeleniumProject.BaseClass;
using SeleniumProject.Elements;
using SeleniumProject.Utility;

namespace SeleniumProject
{
    [TestFixture]
    public class TestPasswordReminderModule : BaseTest
    {
        [Test, Category("Password reminder module")]
        [Description("Test of url correctness after sending email for password reminder")]
        public void TestPasswordReminder()
        {
            LoginModuleElements loginElements = new LoginModuleElements(driver);
            PasswordReminderModuleElements passwordReminderElements = new PasswordReminderModuleElements(driver);

            loginElements.LoginButtoOnDashboard.Click();
            passwordReminderElements.PasswordReminderLink.Click();
            passwordReminderElements.EmailInputInPasswordReminderPage.SendKeys(LoginData.email);
            passwordReminderElements.PasswordReminderButton.Click();

            string actualURL = driver.Url;
            string expectedURL = "https://januszpv.pl/?mod=sendPass&msg=s_resOK";

            Assert.AreEqual(expectedURL, actualURL);
        }
    }
}
