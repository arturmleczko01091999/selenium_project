﻿using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumProject.Elements;
using SeleniumProject.Extensions;
using SeleniumProject.Helpers;

namespace SeleniumProject.Functions
{
    public static class AddProductToCartFunction
    {
        public static void AddProductToCart(IWebDriver driver, string productName)
        {
            AddProductToCartModuleElements elements = new AddProductToCartModuleElements(driver);

            elements.AddToCartButtonOnAddToCartModal.ClickWithWait();
            GoToHelper.GoTo(driver, PageReference.UnshippedCarts);

            elements.ShowAllProductsButtonOnUnshippedCartsPage.Click();

            IWebElement addedProduct = driver.FindElement(By.XPath($".//a[normalize-space() = '{productName}']"), 1000);

            Assert.IsNotNull(addedProduct);
        }
    }
}
