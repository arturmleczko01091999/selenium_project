﻿using OpenQA.Selenium;
using SeleniumProject.Elements;

namespace SeleniumProject.Functions
{
    public static class LoginFunction
    {
        public static void Login(IWebDriver driver, string email, string password)
        {
            LoginModuleElements elements = new LoginModuleElements(driver);

            elements.LoginButtoOnDashboard.Click();
            elements.EmailInputOnLoginPage.SendKeys(email);
            elements.PasswordInputOnLoginPage.SendKeys(password);
            elements.LoginButtonOnLoginPage.Click();
        }
    }
}
