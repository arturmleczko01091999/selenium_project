﻿using OpenQA.Selenium;
using SeleniumProject.Elements;
using SeleniumProject.Extensions;

namespace SeleniumProject.Functions
{
    public static class CreateCollectionFunction
    {
        public static void CreateCollection(IWebDriver driver, string collectionName)
        {
            CollectionsModuleElements elements = new CollectionsModuleElements(driver, 5000);

            elements.CollecionPageLink.Click();
            elements.CreateNewCollectionButtonOnCollectionPage.Click();

            elements.CollectionInputNameOnModal.SendKeysWithWait(collectionName);
            elements.CollectionStatusSwitchOnModal.ClickWithWait();
            elements.SaveCollectionButtonOnModal.ClickWithWait();
        }
    }
}
