﻿using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumProject.Elements;

namespace SeleniumProject.Functions
{
    public static class CheckCollectionExistsFunction
    {
        public static void CheckCollectionExists(IWebDriver driver, string collectionName, int waitTime = 0)
        {
            CollectionsModuleElements elements = new CollectionsModuleElements(driver, 5000);

            if (waitTime != 0)
            {
                Thread.Sleep(waitTime);
            }

            Assert.Greater(elements.CollectionList.Count, 0);
            Assert.AreEqual(elements.CollectionNameOnCollectionPage.Text, collectionName);
        }
    }
}
