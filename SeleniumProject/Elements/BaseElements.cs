﻿using OpenQA.Selenium;

namespace SeleniumProject.Elements
{
    public class BaseElements
    {
        public IWebDriver driver;
        public int timeoutInSeconds;

        public BaseElements(IWebDriver driver, int timeoutInSeconds = 0)
        {
            this.driver = driver;
            this.timeoutInSeconds = timeoutInSeconds;
        }
    }
}
