﻿using OpenQA.Selenium;
using SeleniumProject.Extensions;

namespace SeleniumProject.Elements
{
    public class PasswordReminderModuleElements : BaseElements
    {
        public PasswordReminderModuleElements(IWebDriver driver, int timeoutInSeconds = 0) : base(driver, timeoutInSeconds) { }

        public IWebElement PasswordReminderLink
        {
            get { return driver.FindElement(By.XPath(".//*[@id='login-lostlink']"), timeoutInSeconds); }
        }

        public IWebElement EmailInputInPasswordReminderPage
        {
            get { return driver.FindElement(By.XPath(".//*[@id='lostPass-mail']"), timeoutInSeconds); }
        }

        public IWebElement PasswordReminderButton
        {
            get { return driver.FindElement(By.XPath(".//*[@id='login-btn-remind']"), timeoutInSeconds); }
        }
    }
}
