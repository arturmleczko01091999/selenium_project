﻿using System.Collections.ObjectModel;
using OpenQA.Selenium;
using SeleniumProject.Extensions;

namespace SeleniumProject.Elements
{
    public class AddProductToCartModuleElements : BaseElements
    {
        public AddProductToCartModuleElements(IWebDriver driver, int timeoutInSeconds = 0) : base(driver, timeoutInSeconds) { }
       
        public IWebElement ProductNameElementOnCategoryPage
        {
            get { return driver.FindElement(By.XPath(".//h5[contains(@class, 'product-basics-name')]")); }
        }

        public IWebElement ProductListElement
        {
            get { return driver.FindElement(By.XPath(".//a[contains(@class, 'e2e-categoryView-clickAddProduct')]")); }
        }

        public IWebElement ProductNameOnProductPage
        {
            get { return driver.FindElement(By.XPath(".//*[@id='product-name']")); }
        }

        public IWebElement AddToCartButtonOnProductPage
        {
            get { return driver.FindElement(By.XPath(".//button[contains(@class, 'e2e-productView-addToCartButton')]")); }
        }

        public IWebElement AddToCartButtonOnAddToCartModal
        {
            get { return driver.FindElement(By.XPath(".//button[contains(@class, 'e2e-addToCardModal-addToCardButton')]"), 1000); }
        }

        public IWebElement ShowAllProductsButtonOnUnshippedCartsPage
        {
            get { return driver.FindElement(By.XPath(".//button[contains(@class, 'e2e-cart-showAllProducts-btn')]")); }
        }

        public ReadOnlyCollection<IWebElement> AddToCartButtonsOnCategoryView
        {
            get { return driver.FindElements(By.XPath(".//button[contains(@class, 'e2e-categoryView-productClick')]")); }
        }
    }
}
