﻿using OpenQA.Selenium;
using SeleniumProject.Extensions;

namespace SeleniumProject.Elements
{
    public class CreateCollectionModuleElements : BaseElements
    {
        public CreateCollectionModuleElements(IWebDriver driver, int timeoutInSeconds = 0) : base(driver, timeoutInSeconds) { }

        public IWebElement CreateNewCollectionButtonOnProductListPage
        {
            get { return driver.FindElement(By.XPath(".//button[contains(@class, 'e2e-categoryView-clickToCollection')]"), timeoutInSeconds); }
        }

        public IWebElement CreateNewCollectionButtonOnProductListPageModal
        {
            get { return driver.FindElement(By.XPath(".//button[contains(@class, 'e2e-collectionsList-addCollectionButton')]"), timeoutInSeconds); }
        }

        public IWebElement CollectionNameInputOnProductListPageModal
        {
            get { return driver.FindElement(By.XPath(".//input[contains(@class, 'e2e-collectionsList-inputNameOnModal')]"), timeoutInSeconds); }
        }

        public IWebElement SaveCollectionButtonOnProductListPageModal
        {
            get { return driver.FindElement(By.XPath(".//button[contains(@class, 'e2e-collectionsList-saveCollectionOnModal')]"), timeoutInSeconds); }
        }
    }
}
