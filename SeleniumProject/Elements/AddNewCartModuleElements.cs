﻿using System.Collections.ObjectModel;
using OpenQA.Selenium;
using SeleniumProject.Extensions;

namespace SeleniumProject.Elements
{
    public class AddNewCartModuleElements : BaseElements
    {
        public AddNewCartModuleElements(IWebDriver driver, int timeoutInSeconds = 0) : base(driver, timeoutInSeconds) { }
        
        public IWebElement CartsDropdown
        {
            get { return driver.FindElement(By.XPath(".//button[contains(@class, 'e2e-topMenu-cartsDropdown')]")); }
        }

        public IWebElement UnshippedCartButtonFromDropdown
        {
            get { return driver.FindElement(By.XPath(".//li[contains(@class, 'e2e-topMenu-cartsDropdown-unshippedCart')]")); }
        }

        public IWebElement AddNewBasketButton
        {
            get { return driver.FindElement(By.XPath(".//button[contains(@class, 'e2e-unshippedCarts-addNewBasket')]")); }
        }

        public IWebElement newCartNameInputOnUnshippedCartsPage
        {
            get { return driver.FindElement(By.XPath(".//input[contains(@class, 'e2e-unshippedCarts-newCartName')]")); }
        }

        public IWebElement CreateNewCartButtonOnShippedCartsPage
        {
            get { return driver.FindElement(By.XPath(".//button[contains(@class, 'e2e-unshippedCarts-createNewCart')]")); }
        }

        public IWebElement NewCartNameOnShippedCartsPage
        {
            get { return driver.FindElement(By.XPath(".//h5[contains(@class, 'e2e-unshippedCarts-nameNewCart')]")); }
        }

        public IWebElement DropdownCartList
        {
            get { return driver.FindElement(By.XPath(".//button[contains(@class, 'e2e-addToCartModal-dropdownCartList')]"), 5000); }
        }

        public IWebElement AddNewCartButton
        {
            get { return driver.FindElement(By.XPath(".//span[contains(@class, 'e2e-addToCartModal-addNewCart')]")); }
        }

        public IWebElement NewCartNameInputOnAddToCartModal
        {
            get { return driver.FindElement(By.XPath(".//input[contains(@class, 'e2e-addToCartModal-newCartName')]")); }
        }

        public IWebElement SaveNewCartButton
        {
            get { return driver.FindElement(By.XPath(".//button[contains(@class, 'e2e-addToCartModal-saveCreateNewBasket')]")); }
        }

        public IWebElement AddToCartButtonOnAddToCartModal
        {
            get { return driver.FindElement(By.XPath(".//button[contains(@class, 'e2e-addToCardModal-addToCardButton')]")); }
        }

        public ReadOnlyCollection<IWebElement> AddToCartButtonsOnCategoryView
        {
            get { return driver.FindElements(By.XPath(".//button[contains(@class, 'e2e-categoryView-productClick')]")); }
        }

    }
}
