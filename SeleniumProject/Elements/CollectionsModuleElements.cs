﻿using System.Collections.ObjectModel;
using OpenQA.Selenium;
using SeleniumProject.Extensions;

namespace SeleniumProject.Elements
{
    public class CollectionsModuleElements : BaseElements
    {
        public CollectionsModuleElements(IWebDriver driver, int timeoutInSeconds = 0) : base(driver, timeoutInSeconds) { }

        public IWebElement CollecionPageLink
        {
            get { return driver.FindElement(By.XPath(".//a[contains(@class, 'e2e-topMenu-selectCollection')]"), timeoutInSeconds); }
        }

        public IWebElement CreateNewCollectionButtonOnCollectionPage
        {
            get { return driver.FindElement(By.XPath(".//button[contains(@class, 'e2e-collectionsList-createNewCollection')]"), timeoutInSeconds); }
        }

        public IWebElement CollectionInputNameOnModal
        {
            get { return driver.FindElement(By.XPath(".//input[contains(@class, 'e2e-collectionsList-inputName')]"), timeoutInSeconds); }
        }

        public IWebElement CollectionStatusSwitchOnModal
        {
            get { return driver.FindElement(By.XPath(".//*[@id='collectionStatus']"), timeoutInSeconds); }
        }

        public IWebElement SaveCollectionButtonOnModal
        {
            get { return driver.FindElement(By.XPath(".//button[contains(@class, 'e2e-collectionsList-saveCollection')]"), timeoutInSeconds); }
        }

        public IWebElement CollectionNameOnCollectionPage
        {
            get { return driver.FindElement(By.XPath(".//h6[contains(@class, 'e2e-collectionsList-checkNameCollection')]"), timeoutInSeconds); }
        }

        public ReadOnlyCollection<IWebElement> CollectionList
        {
            get { return driver.FindElements(By.XPath(".//a[contains(@class, 'e2e-collectionsList-toCollection')]"), timeoutInSeconds); }
        }
    }
}
