﻿using OpenQA.Selenium;
using SeleniumProject.Extensions;

namespace SeleniumProject.Elements
{
    public class LoginModuleElements : BaseElements
    {
        public LoginModuleElements(IWebDriver driver, int timeoutInSeconds = 0) : base(driver, timeoutInSeconds) { }

        public IWebElement LoginButtoOnDashboard
        {
            get { return driver.FindElement(By.XPath(".//*[@class='btn btn-link btn-nav-pill d-flex flex-column flex-lg-row  fill-black']"), timeoutInSeconds); }
        }

        public IWebElement EmailInputOnLoginPage
        {
            get { return driver.FindElement(By.XPath(".//*[@id='login-mail']"), timeoutInSeconds); }
        }

        public IWebElement PasswordInputOnLoginPage
        {
            get { return driver.FindElement(By.XPath(".//*[@id='login-pass']"), timeoutInSeconds); }
        }

        public IWebElement LoginButtonOnLoginPage
        {
            get { return driver.FindElement(By.XPath(".//*[@class='btn btn-primary w-1/1 form-login-login-button mb-8']"), timeoutInSeconds); }
        }

        public IWebElement TestAlertMessage
        {
            get { return driver.FindElement(By.XPath(".//*[@class='alert-message m-0 text-gray-800']"), timeoutInSeconds); }
        }
    }
}
