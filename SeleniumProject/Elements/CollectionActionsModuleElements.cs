﻿using OpenQA.Selenium;
using SeleniumProject.Extensions;

namespace SeleniumProject.Elements
{
    public class CollectionActionsModuleElements : BaseElements
    {
        public CollectionActionsModuleElements(IWebDriver driver, int timeoutInSeconds = 0) : base(driver, timeoutInSeconds) { }

        public IWebElement CollectionListElement
        {
            get { return driver.FindElement(By.XPath(".//a[contains(@class, 'e2e-collectionsList-toCollection')]"), timeoutInSeconds); }
        }

        public IWebElement EditCollectionButtonOnEditColletcionPage
        {
            get { return driver.FindElement(By.XPath(".//button[contains(@class, 'e2e-collectionsList-toEditCollection')]"), timeoutInSeconds); }
        }

        public IWebElement DeleteCollectionButtonOnEditModal
        {
            get { return driver.FindElement(By.XPath(".//button[contains(@class, 'e2e-collectionsList-deleteCollection')]"), timeoutInSeconds); }
        }

        public IWebElement ConfirmDeleteCollectionButton
        {
            get { return driver.FindElement(By.XPath(".//button[contains(@class, 'e2e-collectionsList-confirmDeleteCollection')]"), timeoutInSeconds); }
        }

        public IWebElement CollectionActionsButton
        {
            get { return driver.FindElement(By.XPath(".//*[@class='collection_actions d-flex']"), timeoutInSeconds); }
        }

        public IWebElement DuplicateCollectionButton
        {
            get { return driver.FindElement(By.XPath(".//span[normalize-space() = 'Duplikuj']"), timeoutInSeconds); }
        }
    }
}
