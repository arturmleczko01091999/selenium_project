﻿using NUnit.Framework;
using SeleniumProject.BaseClass;
using SeleniumProject.Elements;
using SeleniumProject.Extensions;
using SeleniumProject.Functions;
using SeleniumProject.Helpers;
using SeleniumProject.Utility;

namespace SeleniumProject
{
    [TestFixture]
    public class TestAddNewCartModule : BaseTest
    {
        [Test, Category("Add new cart module")]
        [Description("Test of adding new cart from unshipped carts and checks the correctness of the name")]
        public void TestAddNewCartFromUnshippedCarts()
        {
            AddNewCartModuleElements elements = new AddNewCartModuleElements(driver);

            LoginFunction.Login(driver, LoginData.email, LoginData.password);

            elements.CartsDropdown.Click();
            elements.UnshippedCartButtonFromDropdown.Click();
            elements.AddNewBasketButton.Click();

            string randomCartName = RandomWordHelper.RandomWord(8);

            elements.newCartNameInputOnUnshippedCartsPage.SendKeysWithWait(randomCartName);
            elements.CreateNewCartButtonOnShippedCartsPage.ClickWithWait();

            Assert.AreEqual(elements.NewCartNameOnShippedCartsPage.Text, randomCartName);
        }

        [Test, Category("Add new cart module")]
        [Description("The test adds a new cart from the add to cart modal and checks the correct name")]
        public void TestAddNewCartFromModal()
        {
            AddNewCartModuleElements elements = new AddNewCartModuleElements(driver);

            LoginFunction.Login(driver, LoginData.email, LoginData.password);
            GoToHelper.GoTo(driver, PageReference.ProductList);

            CheckElementExistHelper.CheckElementsExist(elements.AddToCartButtonsOnCategoryView);

            elements.AddToCartButtonsOnCategoryView[0].ClickWithWait();
            elements.DropdownCartList.ClickWithWait();
            elements.AddNewCartButton.ClickWithWait();

            string randomCartName = RandomWordHelper.RandomWord(8);

            elements.NewCartNameInputOnAddToCartModal.SendKeysWithWait(randomCartName);
            elements.SaveNewCartButton.ClickWithWait();
            elements.AddToCartButtonOnAddToCartModal.ClickWithWait();

            GoToHelper.GoTo(driver, PageReference.UnshippedCarts);

            Assert.AreEqual(elements.NewCartNameOnShippedCartsPage.Text, randomCartName);
        }
    }
}
